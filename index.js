Vue.component('test-component', {
  data: function () {
    return {
      property: 'Example property.'
    }
  },
  props: [],
  computed: {
    propertyComputed() {
      return this.property
    }
  },
  beforeCreate: function () {
    console.log('At this point, events and lifecycle have been initialized.')
  },
  created: function() {
    console.log(this.property)
    this.property = 'Example property updated.'
    console.log(this.property)
  },
  template: `
  <div>{{ propertyComputed }}</div>
  `

})

new Vue({
  el: "#app"
})